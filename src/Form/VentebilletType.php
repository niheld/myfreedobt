<?php

namespace App\Form;

use App\Entity\Ventebillet;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VentebilletType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('dateop')
            ->add('client')
            ->add('passager')
            ->add('parcour')
            ->add('depart', DateType::class, array( 'widget' => 'single_text',))
            ->add('retour', DateType::class, array( 'widget' => 'single_text',))
            ->add('fournisseur')
            ->add('amadeus')
            ->add('frais')
            ->add('achat')
            ->add('vente')
            //->add('etatclient')
            //->add('etatfournisseur')
            //->add('etatbillet')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ventebillet::class,
        ]);
    }
}
