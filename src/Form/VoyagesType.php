<?php

namespace App\Form;

use App\Entity\Voyages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoyagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('img')
            ->add('prix')
            ->add('nbrejour')
            ->add('nbrenuitee')
            ->add('inclu1')
            ->add('inclu2')
            ->add('inclu3')
            ->add('inclu4')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Voyages::class,
        ]);
    }
}
