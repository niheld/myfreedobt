<?php

namespace App\Form;

use App\Entity\Stopsale;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StopsaleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('debut', DateType::class, array( 'widget' => 'single_text',))
            ->add('reprise', DateType::class, array( 'widget' => 'single_text',))
            //->add('hotel')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stopsale::class,
        ]);
    }
}
