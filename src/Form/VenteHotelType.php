<?php

namespace App\Form;

use App\Entity\VenteHotel;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;


class VenteHotelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('hotel')
            //->add('user')
            //->add('dateOp')
            ->add('client')
            ->add('arrivee', DateType::class, array( 'widget' => 'single_text',))
            ->add('depart', DateType::class, array( 'widget' => 'single_text',))
            //->add('etatClient')
            //->add('etatVenteHotel')
            //->add('etatFournisseur')
            ->add('remarque', CKEditorType::class)
            //->add('remarque')
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VenteHotel::class,
        ]);
    }
}
