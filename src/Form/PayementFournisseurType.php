<?php

namespace App\Form;

use App\Entity\PayementFournisseur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PayementFournisseurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('dateOp')
            //->add('fournisseur')
            ->add('montantPayement')
            ->add('typepayement')
            ->add('echeance', DateType::class, array( 'widget' => 'single_text',))
            ->add('libelle')
            
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PayementFournisseur::class,
        ]);
    }
}
