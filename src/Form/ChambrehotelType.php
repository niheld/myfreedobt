<?php

namespace App\Form;

use App\Entity\Chambrehotel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChambrehotelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('arrivee')
            ->add('depart')
            ->add('typeChambre')
            ->add('nbreAd')
            ->add('nbreEnf')
            ->add('nbreBb')
            ->add('nomAd1')
            ->add('nomAd2')
            ->add('nomAd3')
            ->add('nomAd4')
            ->add('nomAd5')
            ->add('nomEnf1')
            ->add('ageEnf1')
            ->add('nomEnf2')
            ->add('ageEnf2')
            ->add('nomEnf3')
            ->add('AgeEnf3')
            ->add('nomEnf4')
            ->add('ageEnf4')
            ->add('nomEnf5')
            ->add('ageEnf5')
            ->add('achat')
            ->add('vente')
            ->add('remarques')
            ->add('hotel')
            ->add('reservation')
            ->add('arrangement')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Chambrehotel::class,
        ]);
    }
}
