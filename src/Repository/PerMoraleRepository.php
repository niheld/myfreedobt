<?php

namespace App\Repository;

use App\Entity\PerMorale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PerMorale|null find($id, $lockMode = null, $lockVersion = null)
 * @method PerMorale|null findOneBy(array $criteria, array $orderBy = null)
 * @method PerMorale[]    findAll()
 * @method PerMorale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PerMoraleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PerMorale::class);
    }

//    /**
//     * @return PerMorale[] Returns an array of PerMorale objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PerMorale
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
