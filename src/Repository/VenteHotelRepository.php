<?php

namespace App\Repository;

use App\Entity\VenteHotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VenteHotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method VenteHotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method VenteHotel[]    findAll()
 * @method VenteHotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VenteHotelRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VenteHotel::class);
    }

//    /**
//     * @return VenteHotel[] Returns an array of VenteHotel objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VenteHotel
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
