<?php

namespace App\Repository;

use App\Entity\HomePageCarousel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HomePageCarousel|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomePageCarousel|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomePageCarousel[]    findAll()
 * @method HomePageCarousel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomePageCarouselRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HomePageCarousel::class);
    }

//    /**
//     * @return HomePageCarousel[] Returns an array of HomePageCarousel objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomePageCarousel
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
