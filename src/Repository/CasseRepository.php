<?php

namespace App\Repository;

use App\Entity\Casse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Casse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Casse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Casse[]    findAll()
 * @method Casse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CasseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Casse::class);
    }

//    /**
//     * @return Casse[] Returns an array of Casse objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Casse
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
