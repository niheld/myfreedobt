<?php

namespace App\Repository;

use App\Entity\ClientIndiv;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ClientIndiv|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientIndiv|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientIndiv[]    findAll()
 * @method ClientIndiv[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientIndivRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ClientIndiv::class);
    }

//    /**
//     * @return ClientIndiv[] Returns an array of ClientIndiv objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ClientIndiv
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
