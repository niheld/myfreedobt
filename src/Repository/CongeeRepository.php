<?php

namespace App\Repository;

use App\Entity\Congee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Congee|null find($id, $lockMode = null, $lockVersion = null)
 * @method Congee|null findOneBy(array $criteria, array $orderBy = null)
 * @method Congee[]    findAll()
 * @method Congee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CongeeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Congee::class);
    }

//    /**
//     * @return Congee[] Returns an array of Congee objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Congee
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
