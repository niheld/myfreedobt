<?php

namespace App\Repository;

use App\Entity\MessageTodo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MessageTodo|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageTodo|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageTodo[]    findAll()
 * @method MessageTodo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageTodoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MessageTodo::class);
    }

//    /**
//     * @return MessageTodo[] Returns an array of MessageTodo objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MessageTodo
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
