<?php

namespace App\Repository;

use App\Entity\PaymentHotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PaymentHotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentHotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentHotel[]    findAll()
 * @method PaymentHotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentHotelRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PaymentHotel::class);
    }

//    /**
//     * @return PaymentHotel[] Returns an array of PaymentHotel objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaymentHotel
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
