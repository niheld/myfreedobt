<?php

namespace App\Repository;

use App\Entity\PayementFournisseur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PayementFournisseur|null find($id, $lockMode = null, $lockVersion = null)
 * @method PayementFournisseur|null findOneBy(array $criteria, array $orderBy = null)
 * @method PayementFournisseur[]    findAll()
 * @method PayementFournisseur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PayementFournisseurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PayementFournisseur::class);
    }

//    /**
//     * @return PayementFournisseur[] Returns an array of PayementFournisseur objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PayementFournisseur
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
