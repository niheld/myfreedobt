<?php

namespace App\Repository;

use App\Entity\Ventebillet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ventebillet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ventebillet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ventebillet[]    findAll()
 * @method Ventebillet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VentebilletRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ventebillet::class);
    }

//    /**
//     * @return Ventebillet[] Returns an array of Ventebillet objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ventebillet
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
