<?php

namespace App\Repository;

use App\Entity\Stopsale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Stopsale|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stopsale|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stopsale[]    findAll()
 * @method Stopsale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StopsaleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Stopsale::class);
    }

//    /**
//     * @return Stopsale[] Returns an array of Stopsale objects
//     */
    
    public function findstopsale($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.reprise >= :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?Stopsale
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
