<?php

namespace App\Controller;

use App\Entity\PaymentHotel;
use App\Form\PaymentHotelType;
use App\Repository\PaymentHotelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/payment/hotel")
 */
class PaymentHotelController extends Controller
{
    /**
     * @Route("/", name="payment_hotel_index", methods="GET")
     */
    public function index(PaymentHotelRepository $paymentHotelRepository): Response
    {
        return $this->render('payment_hotel/index.html.twig', ['payment_hotels' => $paymentHotelRepository->findAll()]);
    }

    /**
     * @Route("/new", name="payment_hotel_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $paymentHotel = new PaymentHotel();
        $form = $this->createForm(PaymentHotelType::class, $paymentHotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $paymentHotel->setDateOp(new \Datetime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($paymentHotel);
            $em->flush();

            return $this->redirectToRoute('payment_hotel_index');
        }

        return $this->render('payment_hotel/new.html.twig', [
            'payment_hotel' => $paymentHotel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="payment_hotel_show", methods="GET")
     */
    public function show(PaymentHotel $paymentHotel): Response
    {
        return $this->render('payment_hotel/show.html.twig', ['payment_hotel' => $paymentHotel]);
    }

    /**
     * @Route("/{id}/edit", name="payment_hotel_edit", methods="GET|POST")
     */
    public function edit(Request $request, PaymentHotel $paymentHotel): Response
    {
        $form = $this->createForm(PaymentHotelType::class, $paymentHotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('payment_hotel_edit', ['id' => $paymentHotel->getId()]);
        }

        return $this->render('payment_hotel/edit.html.twig', [
            'payment_hotel' => $paymentHotel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="payment_hotel_delete", methods="DELETE")
     */
    public function delete(Request $request, PaymentHotel $paymentHotel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$paymentHotel->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($paymentHotel);
            $em->flush();
        }

        return $this->redirectToRoute('payment_hotel_index');
    }
}
