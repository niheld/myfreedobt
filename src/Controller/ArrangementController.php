<?php

namespace App\Controller;

use App\Entity\Arrangement;
use App\Form\ArrangementType;
use App\Repository\ArrangementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/arrangement")
 */
class ArrangementController extends Controller
{
    /**
     * @Route("/", name="arrangement_index", methods="GET")
     */
    public function index(ArrangementRepository $arrangementRepository): Response
    {
        return $this->render('arrangement/index.html.twig', ['arrangements' => $arrangementRepository->findAll()]);
    }

    /**
     * @Route("/new", name="arrangement_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $arrangement = new Arrangement();
        $form = $this->createForm(ArrangementType::class, $arrangement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($arrangement);
            $em->flush();

            return $this->redirectToRoute('arrangement_index');
        }

        return $this->render('arrangement/new.html.twig', [
            'arrangement' => $arrangement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="arrangement_show", methods="GET")
     */
    public function show(Arrangement $arrangement): Response
    {
        return $this->render('arrangement/show.html.twig', ['arrangement' => $arrangement]);
    }

    /**
     * @Route("/{id}/edit", name="arrangement_edit", methods="GET|POST")
     */
    public function edit(Request $request, Arrangement $arrangement): Response
    {
        $form = $this->createForm(ArrangementType::class, $arrangement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('arrangement_edit', ['id' => $arrangement->getId()]);
        }

        return $this->render('arrangement/edit.html.twig', [
            'arrangement' => $arrangement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="arrangement_delete", methods="DELETE")
     */
    public function delete(Request $request, Arrangement $arrangement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$arrangement->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($arrangement);
            $em->flush();
        }

        return $this->redirectToRoute('arrangement_index');
    }
}
