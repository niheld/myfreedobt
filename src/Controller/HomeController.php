<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {   
        
        //$json = file_get_contents("https://admin.freedomtravel.tn/prod/json/villejson.php");
        //var_dump($json);
        //var_dump(json_decode($json));
        
        //$name = "ahmed";
        //$message = (new \Swift_Message('Hello Email'))
        //    ->setFrom('contact@my-freedomtravel.com')
        //   ->setTo('ahmed.ourabi@gmail.com')
        //    ->setBody(
         //       $this->renderView(
                    // templates/emails/registration.html.twig
        //            'emails/testmail.html.twig',
        //            array('name' => $name)
        //        ),
        //        'text/html'
        //    )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        //;

        //$this->get('mailer')->send($message);

        
        //$user = $this->getUser()->getId();
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            //'userId' => $this->getUser()->getId(),
        ]);


    }
}
