<?php

namespace App\Controller;

use App\Entity\Chambrehotel;
use App\Form\ChambrehotelType;
use App\Repository\ChambrehotelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/chambrehotel")
 */
class ChambrehotelController extends Controller
{
    /**
     * @Route("/", name="chambrehotel_index", methods="GET")
     */
    public function index(ChambrehotelRepository $chambrehotelRepository): Response
    {
        return $this->render('chambrehotel/index.html.twig', ['chambrehotels' => $chambrehotelRepository->findAll()]);
    }

    /**
     * @Route("/new", name="chambrehotel_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $chambrehotel = new Chambrehotel();
        $form = $this->createForm(ChambrehotelType::class, $chambrehotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($chambrehotel);
            $em->flush();

            return $this->redirectToRoute('chambrehotel_index');
        }

        return $this->render('chambrehotel/new.html.twig', [
            'chambrehotel' => $chambrehotel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="chambrehotel_show", methods="GET")
     */
    public function show(Chambrehotel $chambrehotel): Response
    {
        return $this->render('chambrehotel/show.html.twig', ['chambrehotel' => $chambrehotel]);
    }

    /**
     * @Route("/{id}/edit", name="chambrehotel_edit", methods="GET|POST")
     */
    public function edit(Request $request, Chambrehotel $chambrehotel): Response
    {
        $form = $this->createForm(ChambrehotelType::class, $chambrehotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chambrehotel_edit', ['id' => $chambrehotel->getId()]);
        }

        return $this->render('chambrehotel/edit.html.twig', [
            'chambrehotel' => $chambrehotel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="chambrehotel_delete", methods="DELETE")
     */
    public function delete(Request $request, Chambrehotel $chambrehotel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$chambrehotel->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($chambrehotel);
            $em->flush();
        }

        return $this->redirectToRoute('chambrehotel_index');
    }
}
