<?php

namespace App\Controller;

use App\Entity\TypeFournisseur;
use App\Form\TypeFournisseurType;
use App\Repository\TypeFournisseurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/type/fournisseur")
 */
class TypeFournisseurController extends Controller
{
    /**
     * @Route("/", name="type_fournisseur_index", methods="GET")
     */
    public function index(TypeFournisseurRepository $typeFournisseurRepository): Response
    {
        return $this->render('type_fournisseur/index.html.twig', ['type_fournisseurs' => $typeFournisseurRepository->findAll()]);
    }

    /**
     * @Route("/new", name="type_fournisseur_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $typeFournisseur = new TypeFournisseur();
        $form = $this->createForm(TypeFournisseurType::class, $typeFournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeFournisseur);
            $em->flush();

            return $this->redirectToRoute('type_fournisseur_index');
        }

        return $this->render('type_fournisseur/new.html.twig', [
            'type_fournisseur' => $typeFournisseur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_fournisseur_show", methods="GET")
     */
    public function show(TypeFournisseur $typeFournisseur): Response
    {
        return $this->render('type_fournisseur/show.html.twig', ['type_fournisseur' => $typeFournisseur]);
    }

    /**
     * @Route("/{id}/edit", name="type_fournisseur_edit", methods="GET|POST")
     */
    public function edit(Request $request, TypeFournisseur $typeFournisseur): Response
    {
        $form = $this->createForm(TypeFournisseurType::class, $typeFournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_fournisseur_edit', ['id' => $typeFournisseur->getId()]);
        }

        return $this->render('type_fournisseur/edit.html.twig', [
            'type_fournisseur' => $typeFournisseur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_fournisseur_delete", methods="DELETE")
     */
    public function delete(Request $request, TypeFournisseur $typeFournisseur): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeFournisseur->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeFournisseur);
            $em->flush();
        }

        return $this->redirectToRoute('type_fournisseur_index');
    }
}
