<?php

namespace App\Controller;

use App\Entity\VenteHotel;
use App\Form\VenteHotelType;
use App\Repository\VenteHotelRepository;
use App\Repository\HotelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/vente/hotel")
 */
class VenteHotelController extends Controller
{
    /**
     * @Route("/", name="vente_hotel_index", methods="GET")
     */
    public function index(VenteHotelRepository $venteHotelRepository): Response
    {
        return $this->render('vente_hotel/index.html.twig', 
            ['vente_hotels' => $venteHotelRepository->findBy( [], ['id'=>'DESC'], $limit=null, $offset = null)]

        );
    }

    /**
     * @Route("/new", name="vente_hotel_new", methods="GET|POST")
     */
    public function new(HotelRepository $HotelRepository,Request $request): Response
    {
        $hotel = $HotelRepository->find($request->query->get('hotel'));
        $venteHotel = new VenteHotel();
        $form = $this->createForm(VenteHotelType::class, $venteHotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $venteHotel->setUser($this->getUser());
            $venteHotel->setHotel($hotel);
            //$venteHotel->setClient(1);
            $venteHotel->setEtatClient(0);
            $venteHotel->setEtatFournisseur(0);
            $venteHotel->setEtatVenteHotel(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($venteHotel);
            $em->flush();

            return $this->redirectToRoute('vente_hotel_index');
        }

        return $this->render('vente_hotel/new.html.twig', [
            'vente_hotel' => $venteHotel,
            'form' => $form->createView(),
            'hotel' => $hotel,
        ]);
    }

    /**
     * @Route("/{id}", name="vente_hotel_show", methods="GET")
     */
    public function show(VenteHotel $venteHotel): Response
    {
        return $this->render('vente_hotel/show.html.twig', ['vente_hotel' => $venteHotel]);
    }

    /**
     * @Route("/{id}/edit", name="vente_hotel_edit", methods="GET|POST")
     */
    public function edit(Request $request, VenteHotel $venteHotel): Response
    {
        $form = $this->createForm(VenteHotelType::class, $venteHotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vente_hotel_edit', ['id' => $venteHotel->getId()]);
        }

        return $this->render('vente_hotel/edit.html.twig', [
            'vente_hotel' => $venteHotel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vente_hotel_delete", methods="DELETE")
     */
    public function delete(Request $request, VenteHotel $venteHotel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$venteHotel->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($venteHotel);
            $em->flush();
        }

        return $this->redirectToRoute('vente_hotel_index');
    }
}
