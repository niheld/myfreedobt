<?php

namespace App\Controller;

use App\Entity\PayementFournisseur;
use App\Form\PayementFournisseurType;
use App\Repository\PayementFournisseurRepository;
use App\Repository\FournisseurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/payement/fournisseur")
 */
class PayementFournisseurController extends Controller
{
    /**
     * @Route("/", name="payement_fournisseur_index", methods="GET")
     */
    public function index(FournisseurRepository $FournisseurRepository, PayementFournisseurRepository $payementFournisseurRepository): Response
    {   
        //$fournisseur = $FournisseurRepository->find($request->query->get('fournisseur'));
        return $this->render('payement_fournisseur/index.html.twig', ['payement_fournisseurs' => $payementFournisseurRepository->findBy( [], ['id'=>'DESC'], $limit=null, $offset = null)]
        );
    }

    /**
     * @Route("/etat/{id}", name="payement_fournisseur_etat", methods="GET")
     */
    public function indexByFournisseur($id, FournisseurRepository $FournisseurRepository, PayementFournisseurRepository $payementFournisseurRepository): Response
    {   
        //$fournisseur = $FournisseurRepository->find($id);
        return $this->render('payement_fournisseur/index.html.twig', ['payement_fournisseurs' => $payementFournisseurRepository->findByFournisseur($id)]);
    }

    /**
     * @Route("/new", name="payement_fournisseur_new", methods="GET|POST")
     */
    public function new(FournisseurRepository $FournisseurRepository, Request $request): Response
    {
        $fournisseur = $FournisseurRepository->find($request->query->get('fournisseur'));
        $payementFournisseur = new PayementFournisseur();
        $form = $this->createForm(PayementFournisseurType::class, $payementFournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $payementFournisseur->setDateOp(new \Datetime());
            $payementFournisseur->setFournisseur($fournisseur);
            $em = $this->getDoctrine()->getManager();
            $em->persist($payementFournisseur);
            $em->flush();

            return $this->redirectToRoute('payement_fournisseur_index');
        }

        return $this->render('payement_fournisseur/new.html.twig', [
            'payement_fournisseur' => $payementFournisseur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="payement_fournisseur_show", methods="GET")
     */
    public function show(PayementFournisseur $payementFournisseur): Response
    {
        return $this->render('payement_fournisseur/show.html.twig', ['payement_fournisseur' => $payementFournisseur]);
    }

    /**
     * @Route("/{id}/edit", name="payement_fournisseur_edit", methods="GET|POST")
     */
    public function edit(Request $request, PayementFournisseur $payementFournisseur): Response
    {
        $form = $this->createForm(PayementFournisseurType::class, $payementFournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('payement_fournisseur_edit', ['id' => $payementFournisseur->getId()]);
        }

        return $this->render('payement_fournisseur/edit.html.twig', [
            'payement_fournisseur' => $payementFournisseur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="payement_fournisseur_delete", methods="DELETE")
     */
    public function delete(Request $request, PayementFournisseur $payementFournisseur): Response
    {
        if ($this->isCsrfTokenValid('delete'.$payementFournisseur->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($payementFournisseur);
            $em->flush();
        }

        return $this->redirectToRoute('payement_fournisseur_index');
    }
}
