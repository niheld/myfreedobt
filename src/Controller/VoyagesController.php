<?php

namespace App\Controller;

use App\Entity\Voyages;
use App\Form\VoyagesType;
use App\Repository\VoyagesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/voyages")
 */
class VoyagesController extends Controller
{
    /**
     * @Route("/", name="voyages_index", methods="GET")
     */
    public function index(VoyagesRepository $voyagesRepository): Response
    {
        return $this->render('voyages/index.html.twig', ['voyages' => $voyagesRepository->findAll()]);
    }

    /**
     * @Route("/new", name="voyages_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $voyage = new Voyages();
        $form = $this->createForm(VoyagesType::class, $voyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($voyage);
            $em->flush();

            return $this->redirectToRoute('voyages_index');
        }

        return $this->render('voyages/new.html.twig', [
            'voyage' => $voyage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="voyages_show", methods="GET")
     */
    public function show(Voyages $voyage): Response
    {
        return $this->render('voyages/show.html.twig', ['voyage' => $voyage]);
    }

    /**
     * @Route("/{id}/edit", name="voyages_edit", methods="GET|POST")
     */
    public function edit(Request $request, Voyages $voyage): Response
    {
        $form = $this->createForm(VoyagesType::class, $voyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('voyages_edit', ['id' => $voyage->getId()]);
        }

        return $this->render('voyages/edit.html.twig', [
            'voyage' => $voyage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="voyages_delete", methods="DELETE")
     */
    public function delete(Request $request, Voyages $voyage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$voyage->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($voyage);
            $em->flush();
        }

        return $this->redirectToRoute('voyages_index');
    }
}
