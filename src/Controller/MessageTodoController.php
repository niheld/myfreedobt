<?php

namespace App\Controller;

use App\Entity\MessageTodo;
use App\Form\MessageTodoType;
use App\Repository\MessageTodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/message/todo")
 */
class MessageTodoController extends Controller
{
    /**
     * @Route("/", name="message_todo_index", methods="GET")
     */
    public function index(MessageTodoRepository $messageTodoRepository): Response
    {
        return $this->render('message_todo/index.html.twig', ['message_todos' => $messageTodoRepository->findAll()]);
    }

    /**
     * @Route("/new", name="message_todo_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $messageTodo = new MessageTodo();
        $form = $this->createForm(MessageTodoType::class, $messageTodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($messageTodo);
            $em->flush();

            return $this->redirectToRoute('message_todo_index');
        }

        return $this->render('message_todo/new.html.twig', [
            'message_todo' => $messageTodo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="message_todo_show", methods="GET")
     */
    public function show(MessageTodo $messageTodo): Response
    {
        return $this->render('message_todo/show.html.twig', ['message_todo' => $messageTodo]);
    }

    /**
     * @Route("/{id}/edit", name="message_todo_edit", methods="GET|POST")
     */
    public function edit(Request $request, MessageTodo $messageTodo): Response
    {
        $form = $this->createForm(MessageTodoType::class, $messageTodo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('message_todo_edit', ['id' => $messageTodo->getId()]);
        }

        return $this->render('message_todo/edit.html.twig', [
            'message_todo' => $messageTodo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="message_todo_delete", methods="DELETE")
     */
    public function delete(Request $request, MessageTodo $messageTodo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$messageTodo->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($messageTodo);
            $em->flush();
        }

        return $this->redirectToRoute('message_todo_index');
    }
}
