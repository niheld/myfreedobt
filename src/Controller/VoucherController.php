<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Dompdf\Dompdf;
use Dompdf\Options;
use App\Repository\VilleRepository;

class VoucherController extends Controller
{
    /**
     * @Route("/voucher", name="voucher")
     */
    public function index()
    {
        return $this->render('voucher/index.html.twig', [
            'controller_name' => 'VoucherController',
        ]);
    }
    /**
     * @Route("/admin/voucher/test", name="voucher")
     */
    public function toPdfAction() {
		// On  crée une instance de Dompdf
		$dompdf = new Dompdf();
		//  On  ajoute le texte à afficher
		$dompdf->loadHtml('Hello world');        
		// On fait générer le pdf  à Dompdf ...
		$dompdf->render();
		//  et on l'affiche dans un   objet Response
		return new Response ($dompdf->stream());
	}
	/**
     * @Route("/admin/voucher/testtwig", name="voucher")
     */
	public function toPdfAction2(VilleRepository $villeRepository) {
	    // On récupère l'objet à afficher (rien d'inconnu jusque là)
	    
	    $villes = $villeRepository->findAll();        
	    // On crée une  instance pour définir les options de notre fichier pdf
	    $options = new Options();
	    // Pour simplifier l'affichage des images, on autorise dompdf à utiliser 
	    // des  url pour les nom de  fichier
	    $options->set('isRemoteEnabled', TRUE);
	    // On crée une instance de dompdf avec  les options définies
	    $dompdf = new Dompdf($options);
	    // On demande à Symfony de générer  le code html  correspondant à 
	    // notre template, et on stocke ce code dans une variable
	    $html = $this->renderView(
	      'voucher/reservationHotel.html.twig', 
	      array('villes' => $villes)
	    );
	    // On envoie le code html  à notre instance de dompdf
	    $dompdf->loadHtml($html);        
	    // On demande à dompdf de générer le  pdf
	    $dompdf->render();
	    // On renvoie  le flux du fichier pdf dans une  Response pour l'utilisateur
	    return new Response ($dompdf->stream());
	   }
}
