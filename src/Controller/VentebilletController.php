<?php

namespace App\Controller;

use App\Entity\Ventebillet;
use App\Form\VentebilletType;
use App\Repository\VentebilletRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/ventebillet")
 */
class VentebilletController extends Controller
{
    /**
     * @Route("/", name="ventebillet_index", methods="GET")
     */
    public function index(VentebilletRepository $ventebilletRepository): Response
    {
        return $this->render('ventebillet/index.html.twig', ['ventebillets' => $ventebilletRepository->findBy( [], ['id'=>'DESC'], $limit=null, $offset = null)] 
        );
    }

    /**
     * @Route("/new", name="ventebillet_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $ventebillet = new Ventebillet();
        $form = $this->createForm(VentebilletType::class, $ventebillet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ventebillet->setEtatclient(0);
            $ventebillet->setEtatfournisseur(0);
            $ventebillet->setEtatbillet(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($ventebillet);
            $em->flush();

            return $this->redirectToRoute('ventebillet_index');
        }

        return $this->render('ventebillet/new.html.twig', [
            'ventebillet' => $ventebillet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ventebillet_show", methods="GET")
     */
    public function show(Ventebillet $ventebillet): Response
    {
        return $this->render('ventebillet/show.html.twig', ['ventebillet' => $ventebillet]);
    }

    /**
     * @Route("/{id}/edit", name="ventebillet_edit", methods="GET|POST")
     */
    public function edit(Request $request, Ventebillet $ventebillet): Response
    {
        $form = $this->createForm(VentebilletType::class, $ventebillet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ventebillet_edit', ['id' => $ventebillet->getId()]);
        }

        return $this->render('ventebillet/edit.html.twig', [
            'ventebillet' => $ventebillet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ventebillet_delete", methods="DELETE")
     */
    public function delete(Request $request, Ventebillet $ventebillet): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ventebillet->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ventebillet);
            $em->flush();
        }

        return $this->redirectToRoute('ventebillet_index');
    }
}
