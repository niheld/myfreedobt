<?php

namespace App\Controller;

use App\Entity\Stopsale;
use App\Form\StopsaleType;
use App\Repository\StopsaleRepository;
use App\Repository\HotelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/stopsale")
 */
class StopsaleController extends Controller
{
    /**
     * @Route("/", name="stopsale_index", methods="GET")
     */
    public function index(StopsaleRepository $stopsaleRepository): Response
    {
        $date = new \Datetime();
        return $this->render('stopsale/index.html.twig', 
            //['stopsales' => $stopsaleRepository->findBy( [], ['id'=>'DESC'], $limit=null, $offset = null)]
            //['stopsales' => $stopsaleRepository->findAll()]
            ['stopsales' => $stopsaleRepository->findstopsale($date)]
    );
    }

    /**
     * @Route("/new", name="stopsale_new", methods="GET|POST")
     */
    public function new(HotelRepository $HotelRepository,Request $request): Response
    {
        $hotel = $HotelRepository->find($request->query->get('hotel'));
        $stopsale = new Stopsale();
        $form = $this->createForm(StopsaleType::class, $stopsale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stopsale->setHotel($hotel);
            $em = $this->getDoctrine()->getManager();
            $em->persist($stopsale);
            $em->flush();

            return $this->redirectToRoute('stopsale_index');
        }

        return $this->render('stopsale/new.html.twig', [
            'stopsale' => $stopsale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="stopsale_show", methods="GET")
     */
    public function show(Stopsale $stopsale): Response
    {
        return $this->render('stopsale/show.html.twig', ['stopsale' => $stopsale]);
    }

    /**
     * @Route("/{id}/edit", name="stopsale_edit", methods="GET|POST")
     */
    public function edit(Request $request, Stopsale $stopsale): Response
    {
        $form = $this->createForm(StopsaleType::class, $stopsale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('stopsale_edit', ['id' => $stopsale->getId()]);
        }

        return $this->render('stopsale/edit.html.twig', [
            'stopsale' => $stopsale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="stopsale_delete", methods="DELETE")
     */
    public function delete(Request $request, Stopsale $stopsale): Response
    {
        if ($this->isCsrfTokenValid('delete'.$stopsale->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($stopsale);
            $em->flush();
        }

        return $this->redirectToRoute('stopsale_index');
    }
}
