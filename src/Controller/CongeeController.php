<?php

namespace App\Controller;

use App\Entity\Congee;
use App\Form\CongeeType;
use App\Repository\CongeeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/congee")
 */
class CongeeController extends Controller
{
    /**
     * @Route("/", name="congee_index", methods="GET")
     */
    public function index(CongeeRepository $congeeRepository): Response
    {
        return $this->render('congee/index.html.twig', ['congees' => $congeeRepository->findAll()]);
    }

    /**
     * @Route("/new", name="congee_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {   
        $user = $this->getUser();
        $congee = new Congee();
        $form = $this->createForm(CongeeType::class, $congee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $congee->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($congee);
            $em->flush();

            return $this->redirectToRoute('congee_index');
        }

        return $this->render('congee/new.html.twig', [
            'congee' => $congee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="congee_show", methods="GET")
     */
    public function show(Congee $congee): Response
    {
        return $this->render('congee/show.html.twig', ['congee' => $congee]);
    }

    /**
     * @Route("/{id}/edit", name="congee_edit", methods="GET|POST")
     */
    public function edit(Request $request, Congee $congee): Response
    {
        $form = $this->createForm(CongeeType::class, $congee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('congee_edit', ['id' => $congee->getId()]);
        }

        return $this->render('congee/edit.html.twig', [
            'congee' => $congee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="congee_delete", methods="DELETE")
     */
    public function delete(Request $request, Congee $congee): Response
    {
        if ($this->isCsrfTokenValid('delete'.$congee->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($congee);
            $em->flush();
        }

        return $this->redirectToRoute('congee_index');
    }
}
