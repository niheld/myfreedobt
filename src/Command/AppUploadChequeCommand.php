<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Cheque;
use App\Repository\ChequeRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;

class AppUploadChequeCommand extends Command
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * AppUploadChequeCommand constructor.
     *
     * @param EntityManagerInterface $em
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    protected static $defaultName = 'app:upload-cheque';

    protected function configure()
    {
        $this
            ->setDescription('Upload des cheques')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }
        $chequeRepository = $this->em->getRepository(Cheque::class);
        $reader = Reader::createFromPath('var/csv/cheques.csv');
        $reader->setHeaderOffset(0);
        $records = $reader->getRecords();
        $io->progressStart(iterator_count($records));
        foreach ($records as $offset => $record) {
            $cheque = $chequeRepository->findOneBy( ['numero' => $record['Numero_cheque']] );
            if($cheque === null){
                $cheque = (new Cheque())
                ->setNumero((int)$record['Numero_cheque'])
                //->setFournisseur('test')
                ->setEtat($record['Etat'])
                ->setMontant((int)$record['Montant'])
                ;
                $this->em->persist($cheque);

            }
            $io->progressAdvance();
        }
        // create new athlete
        

        

        // create new Competitor
        //$competitor = (new Competitor())
        //    ->setCategory('category 1')
        //    ->setCompetition('competition 1')
        //;

        //$this->em->persist($competitor);

        // relate the two
        //$athlete->setCompetitor($competitor);

        // save / write the changes to the database
        $this->em->flush();
        $io->progressFinish();
        $io->success('Upload Effectuéavec success');
    }
}
