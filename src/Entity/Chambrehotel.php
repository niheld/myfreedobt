<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChambrehotelRepository")
 * @ApiResource
 */
class Chambrehotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="chambrehotels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $hotel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\VenteHotel", inversedBy="chambrehotels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $reservation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $arrivee;

    /**
     * @ORM\Column(type="datetime")
     */
    private $depart;

    /**
     * @ORM\Column(type="integer")
     */
    private $typeChambre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbreAd;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbreEnf;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbreBb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomAd1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomAd2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomAd3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomAd4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomAd5;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomEnf1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ageEnf1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomEnf2;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ageEnf2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomEnf3;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $AgeEnf3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomEnf4;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ageEnf4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomEnf5;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ageEnf5;

    /**
     * @ORM\Column(type="float")
     */
    private $achat;

    /**
     * @ORM\Column(type="float")
     */
    private $vente;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $remarques;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Arrangement", inversedBy="chambrehotels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $arrangement;

    public function getId()
    {
        return $this->id;
    }

    public function getHotel(): ?Hotel
    {
        return $this->hotel;
    }

    public function setHotel(?Hotel $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function getReservation(): ?VenteHotel
    {
        return $this->reservation;
    }

    public function setReservation(?VenteHotel $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

    public function getArrivee(): ?\DateTimeInterface
    {
        return $this->arrivee;
    }

    public function setArrivee(\DateTimeInterface $arrivee): self
    {
        $this->arrivee = $arrivee;

        return $this;
    }

    public function getDepart(): ?\DateTimeInterface
    {
        return $this->depart;
    }

    public function setDepart(\DateTimeInterface $depart): self
    {
        $this->depart = $depart;

        return $this;
    }

    public function getTypeChambre(): ?int
    {
        return $this->typeChambre;
    }

    public function setTypeChambre(int $typeChambre): self
    {
        $this->typeChambre = $typeChambre;

        return $this;
    }

    public function getNbreAd(): ?int
    {
        return $this->nbreAd;
    }

    public function setNbreAd(?int $nbreAd): self
    {
        $this->nbreAd = $nbreAd;

        return $this;
    }

    public function getNbreEnf(): ?int
    {
        return $this->nbreEnf;
    }

    public function setNbreEnf(?int $nbreEnf): self
    {
        $this->nbreEnf = $nbreEnf;

        return $this;
    }

    public function getNbreBb(): ?int
    {
        return $this->nbreBb;
    }

    public function setNbreBb(?int $nbreBb): self
    {
        $this->nbreBb = $nbreBb;

        return $this;
    }

    public function getNomAd1(): ?string
    {
        return $this->nomAd1;
    }

    public function setNomAd1(?string $nomAd1): self
    {
        $this->nomAd1 = $nomAd1;

        return $this;
    }

    public function getNomAd2(): ?string
    {
        return $this->nomAd2;
    }

    public function setNomAd2(?string $nomAd2): self
    {
        $this->nomAd2 = $nomAd2;

        return $this;
    }

    public function getNomAd3(): ?string
    {
        return $this->nomAd3;
    }

    public function setNomAd3(?string $nomAd3): self
    {
        $this->nomAd3 = $nomAd3;

        return $this;
    }

    public function getNomAd4(): ?string
    {
        return $this->nomAd4;
    }

    public function setNomAd4(?string $nomAd4): self
    {
        $this->nomAd4 = $nomAd4;

        return $this;
    }

    public function getNomAd5(): ?string
    {
        return $this->nomAd5;
    }

    public function setNomAd5(?string $nomAd5): self
    {
        $this->nomAd5 = $nomAd5;

        return $this;
    }

    public function getNomEnf1(): ?string
    {
        return $this->nomEnf1;
    }

    public function setNomEnf1(?string $nomEnf1): self
    {
        $this->nomEnf1 = $nomEnf1;

        return $this;
    }

    public function getAgeEnf1(): ?int
    {
        return $this->ageEnf1;
    }

    public function setAgeEnf1(?int $ageEnf1): self
    {
        $this->ageEnf1 = $ageEnf1;

        return $this;
    }

    public function getNomEnf2(): ?string
    {
        return $this->nomEnf2;
    }

    public function setNomEnf2(?string $nomEnf2): self
    {
        $this->nomEnf2 = $nomEnf2;

        return $this;
    }

    public function getAgeEnf2(): ?int
    {
        return $this->ageEnf2;
    }

    public function setAgeEnf2(?int $ageEnf2): self
    {
        $this->ageEnf2 = $ageEnf2;

        return $this;
    }

    public function getNomEnf3(): ?string
    {
        return $this->nomEnf3;
    }

    public function setNomEnf3(?string $nomEnf3): self
    {
        $this->nomEnf3 = $nomEnf3;

        return $this;
    }

    public function getAgeEnf3(): ?int
    {
        return $this->AgeEnf3;
    }

    public function setAgeEnf3(?int $AgeEnf3): self
    {
        $this->AgeEnf3 = $AgeEnf3;

        return $this;
    }

    public function getNomEnf4(): ?string
    {
        return $this->nomEnf4;
    }

    public function setNomEnf4(?string $nomEnf4): self
    {
        $this->nomEnf4 = $nomEnf4;

        return $this;
    }

    public function getAgeEnf4(): ?int
    {
        return $this->ageEnf4;
    }

    public function setAgeEnf4(?int $ageEnf4): self
    {
        $this->ageEnf4 = $ageEnf4;

        return $this;
    }

    public function getNomEnf5(): ?string
    {
        return $this->nomEnf5;
    }

    public function setNomEnf5(?string $nomEnf5): self
    {
        $this->nomEnf5 = $nomEnf5;

        return $this;
    }

    public function getAgeEnf5(): ?int
    {
        return $this->ageEnf5;
    }

    public function setAgeEnf5(?int $ageEnf5): self
    {
        $this->ageEnf5 = $ageEnf5;

        return $this;
    }

    public function getAchat(): ?float
    {
        return $this->achat;
    }

    public function setAchat(float $achat): self
    {
        $this->achat = $achat;

        return $this;
    }

    public function getVente(): ?float
    {
        return $this->vente;
    }

    public function setVente(float $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getRemarques(): ?string
    {
        return $this->remarques;
    }

    public function setRemarques(?string $remarques): self
    {
        $this->remarques = $remarques;

        return $this;
    }

    public function getArrangement(): ?Arrangement
    {
        return $this->arrangement;
    }

    public function setArrangement(?Arrangement $arrangement): self
    {
        $this->arrangement = $arrangement;

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return strval($this->getid());
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }
}
