<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaysRepository")
 * @ApiResource(attributes={"pagination_enabled"=false},
 *              normalizationContext={"groups"={"pays"}, "enable_max_depth"=true})
 */
class Pays
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("pays")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("pays")
     * @Groups("per_morale")
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PerMorale", mappedBy="pays")
     */
    private $perMorales;

    public function __construct()
    {
        $this->perMorales = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|PerMorale[]
     */
    public function getPerMorales(): Collection
    {
        return $this->perMorales;
    }

    public function addPerMorale(PerMorale $perMorale): self
    {
        if (!$this->perMorales->contains($perMorale)) {
            $this->perMorales[] = $perMorale;
            $perMorale->setPays($this);
        }

        return $this;
    }

    public function removePerMorale(PerMorale $perMorale): self
    {
        if ($this->perMorales->contains($perMorale)) {
            $this->perMorales->removeElement($perMorale);
            // set the owning side to null (unless already changed)
            if ($perMorale->getPays() === $this) {
                $perMorale->setPays(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return $this->getLibelle();
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }
}
