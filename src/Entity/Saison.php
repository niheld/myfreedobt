<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SaisonRepository")
 * @ApiResource
 */
class Saison
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $debut;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $fin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contrat", inversedBy="saisons")
     */
    private $contrat;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Arrangement", inversedBy="saisons")
     */
    private $arrangement;

    public function __construct()
    {
        $this->arrangement = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(?\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(?\DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getContrat(): ?Contrat
    {
        return $this->contrat;
    }

    public function setContrat(?Contrat $contrat): self
    {
        $this->contrat = $contrat;

        return $this;
    }
    //public function getSaison(){
    //    return strval( $this->getDebut().' '.$this->getFin() );
    //}
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return strval($this->getId());
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }

    /**
     * @return Collection|Arrangement[]
     */
    public function getArrangement(): Collection
    {
        return $this->arrangement;
    }

    public function addArrangement(Arrangement $arrangement): self
    {
        if (!$this->arrangement->contains($arrangement)) {
            $this->arrangement[] = $arrangement;
        }

        return $this;
    }

    public function removeArrangement(Arrangement $arrangement): self
    {
        if ($this->arrangement->contains($arrangement)) {
            $this->arrangement->removeElement($arrangement);
        }

        return $this;
    }
}
