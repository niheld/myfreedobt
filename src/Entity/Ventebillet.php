<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VentebilletRepository")
 * @ApiResource(attributes={"pagination_enabled"=false})
 */
class Ventebillet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateop;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $passager;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $parcour;

    /**
     * @ORM\Column(type="date")
     */
    private $depart;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $retour;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $amadeus;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $frais;

    /**
     * @ORM\Column(type="float")
     */
    private $achat;

    /**
     * @ORM\Column(type="float")
     */
    private $vente;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatclient;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatfournisseur;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatbillet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Fournisseur", inversedBy="ventebillets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fournisseur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="ventebillets")
     */
    private $client;

    public function __construct(){
        $this->dateop = $this->depart  = new \Datetime();
        //= $this->retour
        $this->etatclient = $this->etatfournisseur = $this->etatbillet = false ; 
        
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDateop(): ?\DateTimeInterface
    {
        return $this->dateop;
    }

    public function setDateop(\DateTimeInterface $dateop): self
    {
        $this->dateop = $dateop;

        return $this;
    }

    public function getPassager(): ?string
    {
        return $this->passager;
    }

    public function setPassager(string $passager): self
    {
        $this->passager = $passager;

        return $this;
    }

    public function getParcour(): ?string
    {
        return $this->parcour;
    }

    public function setParcour(string $parcour): self
    {
        $this->parcour = $parcour;

        return $this;
    }

    public function getDepart(): ?\DateTimeInterface
    {
        return $this->depart;
    }

    public function setDepart(\DateTimeInterface $depart): self
    {
        $this->depart = $depart;

        return $this;
    }

    public function getRetour(): ?\DateTimeInterface
    {
        return $this->retour;
    }

    public function setRetour(?\DateTimeInterface $retour): self
    {
        $this->retour = $retour;

        return $this;
    }

    public function getAmadeus(): ?float
    {
        return $this->amadeus;
    }

    public function setAmadeus(?float $amadeus): self
    {
        $this->amadeus = $amadeus;

        return $this;
    }

    public function getFrais(): ?float
    {
        return $this->frais;
    }

    public function setFrais(?float $frais): self
    {
        $this->frais = $frais;

        return $this;
    }

    public function getAchat(): ?float
    {
        return $this->achat;
    }

    public function setAchat(float $achat): self
    {
        $this->achat = $achat;

        return $this;
    }

    public function getVente(): ?float
    {
        return $this->vente;
    }

    public function setVente(float $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getEtatclient(): ?bool
    {
        return $this->etatclient;
    }

    public function setEtatclient(?bool $etatclient): self
    {
        $this->etatclient = $etatclient;

        return $this;
    }

    public function getEtatfournisseur(): ?bool
    {
        return $this->etatfournisseur;
    }

    public function setEtatfournisseur(?bool $etatfournisseur): self
    {
        $this->etatfournisseur = $etatfournisseur;

        return $this;
    }

    public function getEtatbillet(): ?bool
    {
        return $this->etatbillet;
    }

    public function setEtatbillet(?bool $etatbillet): self
    {
        $this->etatbillet = $etatbillet;

        return $this;
    }

    public function getFournisseur(): ?Fournisseur
    {
        return $this->fournisseur;
    }

    public function setFournisseur(?Fournisseur $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return strval($this->getid());
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }
}
