<?php
// src/App/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ApiResource
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Congee", mappedBy="user", orphanRemoval=true)
     */
    private $congees;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VenteHotel", mappedBy="user")
     */
    private $venteHotels;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tarif", mappedBy="user")
     */
    private $tarifs;

    

    public function __construct()
    {
        parent::__construct();
        $this->congees = new ArrayCollection();
        $this->venteHotels = new ArrayCollection();
        $this->tarifs = new ArrayCollection();
        
        // your own logic
    }

    /**
     * @return Collection|Congee[]
     */
    public function getCongees(): Collection
    {
        return $this->congees;
    }

    public function addCongee(Congee $congee): self
    {
        if (!$this->congees->contains($congee)) {
            $this->congees[] = $congee;
            $congee->setUser($this);
        }

        return $this;
    }

    public function removeCongee(Congee $congee): self
    {
        if ($this->congees->contains($congee)) {
            $this->congees->removeElement($congee);
            // set the owning side to null (unless already changed)
            if ($congee->getUser() === $this) {
                $congee->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|VenteHotel[]
     */
    public function getVenteHotels(): Collection
    {
        return $this->venteHotels;
    }

    public function addVenteHotel(VenteHotel $venteHotel): self
    {
        if (!$this->venteHotels->contains($venteHotel)) {
            $this->venteHotels[] = $venteHotel;
            $venteHotel->setUser($this);
        }

        return $this;
    }

    public function removeVenteHotel(VenteHotel $venteHotel): self
    {
        if ($this->venteHotels->contains($venteHotel)) {
            $this->venteHotels->removeElement($venteHotel);
            // set the owning side to null (unless already changed)
            if ($venteHotel->getUser() === $this) {
                $venteHotel->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tarif[]
     */
    public function getTarifs(): Collection
    {
        return $this->tarifs;
    }

    public function addTarif(Tarif $tarif): self
    {
        if (!$this->tarifs->contains($tarif)) {
            $this->tarifs[] = $tarif;
            $tarif->addUser($this);
        }

        return $this;
    }

    public function removeTarif(Tarif $tarif): self
    {
        if ($this->tarifs->contains($tarif)) {
            $this->tarifs->removeElement($tarif);
            $tarif->removeUser($this);
        }

        return $this;
    }

    
}