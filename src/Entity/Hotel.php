<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 * @Vich\Uploadable
 * @ApiResource(attributes={"pagination_enabled"=false},
 *              normalizationContext={"groups"={"hotel"}})
 */
class Hotel
{
    /**
     * @Groups("hotel")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("hotel")
     * @ORM\Column(type="string", length=255)
     * @Groups("ville")
     */
    private $nom;

    /**
     * @ORM\Column(type="smallint")
     * @Groups("hotel")
     * @Groups("ville")
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="hotels")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("hotel")
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VenteHotel", mappedBy="hotel")
     */
    private $venteHotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stopsale", mappedBy="hotel", orphanRemoval=true)
     * @Groups("hotel")
     * @Groups("ville")
     */
    private $stopsales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chambrehotel", mappedBy="hotel")
     */
    private $chambrehotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PaymentHotel", mappedBy="hotel")
     */
    private $paymentHotels;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="hotel_miniature", fileNameProperty="imageName", size="imageSize")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cheque", mappedBy="hotel")
     */
    private $cheques;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contrat", mappedBy="hotel")
     */
    private $contrats;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("hotel")
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("hotel")
     */
    private $img;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("hotel")
     */
    private $descriptif;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sts;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cyberesa;

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getupdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }
    
    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }


    public function __construct()
    {
        $this->venteHotels = new ArrayCollection();
        $this->stopsales = new ArrayCollection();
        $this->chambrehotels = new ArrayCollection();
        $this->paymentHotels = new ArrayCollection();
        $this->cheques = new ArrayCollection();
        $this->contrats = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCategorie(): ?int
    {
        return $this->categorie;
    }

    public function setCategorie(int $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|VenteHotel[]
     */
    public function getVenteHotels(): Collection
    {
        return $this->venteHotels;
    }

    public function addVenteHotel(VenteHotel $venteHotel): self
    {
        if (!$this->venteHotels->contains($venteHotel)) {
            $this->venteHotels[] = $venteHotel;
            $venteHotel->setHotel($this);
        }

        return $this;
    }

    public function removeVenteHotel(VenteHotel $venteHotel): self
    {
        if ($this->venteHotels->contains($venteHotel)) {
            $this->venteHotels->removeElement($venteHotel);
            // set the owning side to null (unless already changed)
            if ($venteHotel->getHotel() === $this) {
                $venteHotel->setHotel(null);
            }
        }

        return $this;
    }

    public function __toString(){
        // sert a afficher le nom de l'hotel
        return $this->getNom();
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }

    /**
     * @return Collection|Stopsale[]
     */
    public function getStopsales(): Collection
    {
        return $this->stopsales;
    }

    public function addStopsale(Stopsale $stopsale): self
    {
        if (!$this->stopsales->contains($stopsale)) {
            $this->stopsales[] = $stopsale;
            $stopsale->setHotel($this);
        }

        return $this;
    }

    public function removeStopsale(Stopsale $stopsale): self
    {
        if ($this->stopsales->contains($stopsale)) {
            $this->stopsales->removeElement($stopsale);
            // set the owning side to null (unless already changed)
            if ($stopsale->getHotel() === $this) {
                $stopsale->setHotel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Chambrehotel[]
     */
    public function getChambrehotels(): Collection
    {
        return $this->chambrehotels;
    }

    public function addChambrehotel(Chambrehotel $chambrehotel): self
    {
        if (!$this->chambrehotels->contains($chambrehotel)) {
            $this->chambrehotels[] = $chambrehotel;
            $chambrehotel->setHotel($this);
        }

        return $this;
    }

    public function removeChambrehotel(Chambrehotel $chambrehotel): self
    {
        if ($this->chambrehotels->contains($chambrehotel)) {
            $this->chambrehotels->removeElement($chambrehotel);
            // set the owning side to null (unless already changed)
            if ($chambrehotel->getHotel() === $this) {
                $chambrehotel->setHotel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PaymentHotel[]
     */
    public function getPaymentHotels(): Collection
    {
        return $this->paymentHotels;
    }

    public function addPaymentHotel(PaymentHotel $paymentHotel): self
    {
        if (!$this->paymentHotels->contains($paymentHotel)) {
            $this->paymentHotels[] = $paymentHotel;
            $paymentHotel->setHotel($this);
        }

        return $this;
    }

    public function removePaymentHotel(PaymentHotel $paymentHotel): self
    {
        if ($this->paymentHotels->contains($paymentHotel)) {
            $this->paymentHotels->removeElement($paymentHotel);
            // set the owning side to null (unless already changed)
            if ($paymentHotel->getHotel() === $this) {
                $paymentHotel->setHotel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cheque[]
     */
    public function getCheques(): Collection
    {
        return $this->cheques;
    }

    public function addCheque(Cheque $cheque): self
    {
        if (!$this->cheques->contains($cheque)) {
            $this->cheques[] = $cheque;
            $cheque->setHotel($this);
        }

        return $this;
    }

    public function removeCheque(Cheque $cheque): self
    {
        if ($this->cheques->contains($cheque)) {
            $this->cheques->removeElement($cheque);
            // set the owning side to null (unless already changed)
            if ($cheque->getHotel() === $this) {
                $cheque->setHotel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contrat[]
     */
    public function getContrats(): Collection
    {
        return $this->contrats;
    }

    public function addContrat(Contrat $contrat): self
    {
        if (!$this->contrats->contains($contrat)) {
            $this->contrats[] = $contrat;
            $contrat->setHotel($this);
        }

        return $this;
    }

    public function removeContrat(Contrat $contrat): self
    {
        if ($this->contrats->contains($contrat)) {
            $this->contrats->removeElement($contrat);
            // set the owning side to null (unless already changed)
            if ($contrat->getHotel() === $this) {
                $contrat->setHotel(null);
            }
        }

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getDescriptif(): ?string
    {
        return $this->descriptif;
    }

    public function setDescriptif(?string $descriptif): self
    {
        $this->descriptif = $descriptif;

        return $this;
    }

    public function getSts(): ?int
    {
        return $this->sts;
    }

    public function setSts(?int $sts): self
    {
        $this->sts = $sts;

        return $this;
    }

    public function getCyberesa(): ?int
    {
        return $this->cyberesa;
    }

    public function setCyberesa(?int $cyberesa): self
    {
        $this->cyberesa = $cyberesa;

        return $this;
    }

    
}
