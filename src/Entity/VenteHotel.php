<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VenteHotelRepository")
 * @ApiResource(attributes={"pagination_enabled"=false})
 */
class VenteHotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOp;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="venteHotels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $hotel;

    /**
     * @ORM\Column(type="date")
     */
    private $arrivee;

    /**
     * @ORM\Column(type="date")
     */
    private $depart;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatClient;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatVenteHotel;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatFournisseur;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $remarque;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="venteHotels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chambrehotel", mappedBy="reservation")
     */
    private $chambrehotels;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="venteHotels")
     */
    private $client;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $vente;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ClientIndiv", inversedBy="venteHotels")
     */
    private $clientIndiv;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $idBrave;

    public function __construct()
    {
        $this->chambrehotels = new ArrayCollection();
        $this->dateOp = new \Datetime();
        $this->etatClient = false;
        $this->etatVenteHotel = false;
        $this->etatFournisseur = false;
        $this->arrivee = new \Datetime();
        $this->depart = new \Datetime();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getDateOp(): ?\DateTimeInterface
    {
        return $this->dateOp;
    }

    public function setDateOp(\DateTimeInterface $dateOp): self
    {
        $this->dateOp = $dateOp;

        return $this;
    }

    

    public function getHotel(): ?hotel
    {
        return $this->hotel;
    }

    public function setHotel(?hotel $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function getArrivee(): ?\DateTimeInterface
    {
        return $this->arrivee;
    }

    public function setArrivee(\DateTimeInterface $arrivee): self
    {
        $this->arrivee = $arrivee;

        return $this;
    }

    public function getDepart(): ?\DateTimeInterface
    {
        return $this->depart;
    }

    public function setDepart(\DateTimeInterface $depart): self
    {
        $this->depart = $depart;

        return $this;
    }

    public function getEtatClient(): ?bool
    {
        return $this->etatClient;
    }

    public function setEtatClient(bool $etatClient): self
    {
        $this->etatClient = $etatClient;

        return $this;
    }

    public function getEtatVenteHotel(): ?bool
    {
        return $this->etatVenteHotel;
    }

    public function setEtatVenteHotel(?bool $etatVenteHotel): self
    {
        $this->etatVenteHotel = $etatVenteHotel;

        return $this;
    }

    public function getEtatFournisseur(): ?bool
    {
        return $this->etatFournisseur;
    }

    public function setEtatFournisseur(?bool $etatFournisseur): self
    {
        $this->etatFournisseur = $etatFournisseur;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Chambrehotel[]
     */
    public function getChambrehotels(): Collection
    {
        return $this->chambrehotels;
    }

    public function addChambrehotel(Chambrehotel $chambrehotel): self
    {
        if (!$this->chambrehotels->contains($chambrehotel)) {
            $this->chambrehotels[] = $chambrehotel;
            $chambrehotel->setReservation($this);
        }

        return $this;
    }

    public function removeChambrehotel(Chambrehotel $chambrehotel): self
    {
        if ($this->chambrehotels->contains($chambrehotel)) {
            $this->chambrehotels->removeElement($chambrehotel);
            // set the owning side to null (unless already changed)
            if ($chambrehotel->getReservation() === $this) {
                $chambrehotel->setReservation(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return strval($this->getid());
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getAchat(): ?float
    {
        return $this->achat;
    }

    public function setAchat(?float $achat): self
    {
        $this->achat = $achat;

        return $this;
    }

    public function getVente(): ?float
    {
        return $this->vente;
    }

    public function setVente(?float $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getClientIndiv(): ?ClientIndiv
    {
        return $this->clientIndiv;
    }

    public function setClientIndiv(?ClientIndiv $clientIndiv): self
    {
        $this->clientIndiv = $clientIndiv;

        return $this;
    }

    public function getIdBrave(): ?string
    {
        return $this->idBrave;
    }

    public function setIdBrave(?string $idBrave): self
    {
        $this->idBrave = $idBrave;

        return $this;
    }
}
