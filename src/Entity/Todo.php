<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TodoRepository")
 */
class Todo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sujet;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MessageTodo", mappedBy="todo")
     */
    private $messageTodos;

    public function __construct()
    {
        $this->messageTodos = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    

    public function getSujet(): ?string
    {
        return $this->sujet;
    }

    public function setSujet(string $sujet): self
    {
        $this->sujet = $sujet;

        return $this;
    }

    /**
     * @return Collection|MessageTodo[]
     */
    public function getMessageTodos(): Collection
    {
        return $this->messageTodos;
    }

    public function addMessageTodo(MessageTodo $messageTodo): self
    {
        if (!$this->messageTodos->contains($messageTodo)) {
            $this->messageTodos[] = $messageTodo;
            $messageTodo->setTodo($this);
        }

        return $this;
    }

    public function removeMessageTodo(MessageTodo $messageTodo): self
    {
        if ($this->messageTodos->contains($messageTodo)) {
            $this->messageTodos->removeElement($messageTodo);
            // set the owning side to null (unless already changed)
            if ($messageTodo->getTodo() === $this) {
                $messageTodo->setTodo(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return $this->getSujet();
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }
}
