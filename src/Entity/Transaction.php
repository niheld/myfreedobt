<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $nature;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypePayement", inversedBy="transactions")
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     */
    private $solde;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tresorie")
     */
    private $caisseDepart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tresorie")
     */
    private $caisseDestination;

    public function getId()
    {
        return $this->id;
    }

    public function getNature(): ?bool
    {
        return $this->nature;
    }

    public function setNature(bool $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    public function getType(): ?TypePayement
    {
        return $this->type;
    }

    public function setType(?TypePayement $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSolde(): ?float
    {
        return $this->solde;
    }

    public function setSolde(float $solde): self
    {
        $this->solde = $solde;

        return $this;
    }

    public function getCaisseDepart(): ?Tresorie
    {
        return $this->caisseDepart;
    }

    public function setCaisseDepart(?Tresorie $caisseDepart): self
    {
        $this->caisseDepart = $caisseDepart;

        return $this;
    }

    public function getCaisseDestination(): ?Tresorie
    {
        return $this->caisseDestination;
    }

    public function setCaisseDestination(?Tresorie $caisseDestination): self
    {
        $this->caisseDestination = $caisseDestination;

        return $this;
    }
}
