<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientIndivRepository")
 * @ApiResource
 */
class ClientIndiv
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $tel;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Client", inversedBy="clientIndivs")
     */
    private $b2b;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VenteHotel", mappedBy="clientIndiv")
     */
    private $venteHotels;

    public function __construct()
    {
        $this->b2b = new ArrayCollection();
        $this->venteHotels = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTel(): ?int
    {
        return $this->tel;
    }

    public function setTel(int $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getB2b(): Collection
    {
        return $this->b2b;
    }

    public function addB2b(Client $b2b): self
    {
        if (!$this->b2b->contains($b2b)) {
            $this->b2b[] = $b2b;
        }

        return $this;
    }

    public function removeB2b(Client $b2b): self
    {
        if ($this->b2b->contains($b2b)) {
            $this->b2b->removeElement($b2b);
        }

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'entité'
        return $this->getNom();
        // Pour afficher l'id de la ville : return $this->$nom;
        
    }

    /**
     * @return Collection|VenteHotel[]
     */
    public function getVenteHotels(): Collection
    {
        return $this->venteHotels;
    }

    public function addVenteHotel(VenteHotel $venteHotel): self
    {
        if (!$this->venteHotels->contains($venteHotel)) {
            $this->venteHotels[] = $venteHotel;
            $venteHotel->setClientIndiv($this);
        }

        return $this;
    }

    public function removeVenteHotel(VenteHotel $venteHotel): self
    {
        if ($this->venteHotels->contains($venteHotel)) {
            $this->venteHotels->removeElement($venteHotel);
            // set the owning side to null (unless already changed)
            if ($venteHotel->getClientIndiv() === $this) {
                $venteHotel->setClientIndiv(null);
            }
        }

        return $this;
    }
}
