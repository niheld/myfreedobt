<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentHotelRepository")
 * @ApiResource
 */
class PaymentHotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOp;

    /**
     * @ORM\Column(type="float")
     */
    private $montantPayement;

    /**
     * @ORM\Column(type="datetime")
     */
    private $echeance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypePayement", inversedBy="paymentHotels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typepayement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="paymentHotels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $hotel;

    public function getId()
    {
        return $this->id;
    }

    public function getDateOp(): ?\DateTimeInterface
    {
        return $this->dateOp;
    }

    public function setDateOp(\DateTimeInterface $dateOp): self
    {
        $this->dateOp = $dateOp;

        return $this;
    }

    public function getMontantPayement(): ?float
    {
        return $this->montantPayement;
    }

    public function setMontantPayement(float $montantPayement): self
    {
        $this->montantPayement = $montantPayement;

        return $this;
    }

    public function getEcheance(): ?\DateTimeInterface
    {
        return $this->echeance;
    }

    public function setEcheance(\DateTimeInterface $echeance): self
    {
        $this->echeance = $echeance;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getTypepayement(): ?TypePayement
    {
        return $this->typepayement;
    }

    public function setTypepayement(?TypePayement $typepayement): self
    {
        $this->typepayement = $typepayement;

        return $this;
    }

    public function getHotel(): ?Hotel
    {
        return $this->hotel;
    }

    public function setHotel(?Hotel $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }
}
