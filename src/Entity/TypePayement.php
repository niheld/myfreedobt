<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypePayementRepository")
 * @ApiResource
 */
class TypePayement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PayementFournisseur", mappedBy="typepayement")
     */
    private $payementFournisseurs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PaymentHotel", mappedBy="typepayement")
     */
    private $paymentHotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="type")
     */
    private $transactions;

    public function __construct()
    {
        $this->payementFournisseurs = new ArrayCollection();
        $this->paymentHotels = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return $this->getNom();
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }

    /**
     * @return Collection|PayementFournisseur[]
     */
    public function getPayementFournisseurs(): Collection
    {
        return $this->payementFournisseurs;
    }

    public function addPayementFournisseur(PayementFournisseur $payementFournisseur): self
    {
        if (!$this->payementFournisseurs->contains($payementFournisseur)) {
            $this->payementFournisseurs[] = $payementFournisseur;
            $payementFournisseur->setTypepayement($this);
        }

        return $this;
    }

    public function removePayementFournisseur(PayementFournisseur $payementFournisseur): self
    {
        if ($this->payementFournisseurs->contains($payementFournisseur)) {
            $this->payementFournisseurs->removeElement($payementFournisseur);
            // set the owning side to null (unless already changed)
            if ($payementFournisseur->getTypepayement() === $this) {
                $payementFournisseur->setTypepayement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PaymentHotel[]
     */
    public function getPaymentHotels(): Collection
    {
        return $this->paymentHotels;
    }

    public function addPaymentHotel(PaymentHotel $paymentHotel): self
    {
        if (!$this->paymentHotels->contains($paymentHotel)) {
            $this->paymentHotels[] = $paymentHotel;
            $paymentHotel->setTypepayement($this);
        }

        return $this;
    }

    public function removePaymentHotel(PaymentHotel $paymentHotel): self
    {
        if ($this->paymentHotels->contains($paymentHotel)) {
            $this->paymentHotels->removeElement($paymentHotel);
            // set the owning side to null (unless already changed)
            if ($paymentHotel->getTypepayement() === $this) {
                $paymentHotel->setTypepayement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setType($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getType() === $this) {
                $transaction->setType(null);
            }
        }

        return $this;
    }
}
