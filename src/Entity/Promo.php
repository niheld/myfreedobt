<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromoRepository")
 * @ApiResource
 */
class Promo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $debut_validite;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $fin_validite;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $debut;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $fin;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contrat", inversedBy="promos")
     */
    private $contrat;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Arrangement", inversedBy="promos")
     */
    private $arrangement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $affichePromo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatAffichePromo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Fournisseur", mappedBy="promo")
     */
    private $fournisseur;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Tarif", mappedBy="promo", cascade={"persist", "remove"})
     */
    private $tarif;

    public function __construct()
    {
        $this->arrangement = new ArrayCollection();
        $this->etatAffichePromo = false;
        $this->fournisseur = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDebutValidite(): ?\DateTimeInterface
    {
        return $this->debut_validite;
    }

    public function setDebutValidite(?\DateTimeInterface $debut_validite): self
    {
        $this->debut_validite = $debut_validite;

        return $this;
    }

    public function getFinValidite(): ?\DateTimeInterface
    {
        return $this->fin_validite;
    }

    public function setFinValidite(?\DateTimeInterface $fin_validite): self
    {
        $this->fin_validite = $fin_validite;

        return $this;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(?\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(?\DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getContrat(): ?Contrat
    {
        return $this->contrat;
    }

    public function setContrat(?Contrat $contrat): self
    {
        $this->contrat = $contrat;

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return strval($this->getId());
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }

    /**
     * @return Collection|Arrangement[]
     */
    public function getArrangement(): Collection
    {
        return $this->arrangement;
    }

    public function addArrangement(Arrangement $arrangement): self
    {
        if (!$this->arrangement->contains($arrangement)) {
            $this->arrangement[] = $arrangement;
        }

        return $this;
    }

    public function removeArrangement(Arrangement $arrangement): self
    {
        if ($this->arrangement->contains($arrangement)) {
            $this->arrangement->removeElement($arrangement);
        }

        return $this;
    }

    public function getAffichePromo(): ?string
    {
        return $this->affichePromo;
    }

    public function setAffichePromo(?string $affichePromo): self
    {
        $this->affichePromo = $affichePromo;

        return $this;
    }

    public function getEtatAffichePromo(): ?bool
    {
        return $this->etatAffichePromo;
    }

    public function setEtatAffichePromo(?bool $etatAffichePromo): self
    {
        $this->etatAffichePromo = $etatAffichePromo;

        return $this;
    }

    /**
     * @return Collection|Fournisseur[]
     */
    public function getFournisseur(): Collection
    {
        return $this->fournisseur;
    }

    public function addFournisseur(Fournisseur $fournisseur): self
    {
        if (!$this->fournisseur->contains($fournisseur)) {
            $this->fournisseur[] = $fournisseur;
            $fournisseur->setPromo($this);
        }

        return $this;
    }

    public function removeFournisseur(Fournisseur $fournisseur): self
    {
        if ($this->fournisseur->contains($fournisseur)) {
            $this->fournisseur->removeElement($fournisseur);
            // set the owning side to null (unless already changed)
            if ($fournisseur->getPromo() === $this) {
                $fournisseur->setPromo(null);
            }
        }

        return $this;
    }

    public function getTarif(): ?Tarif
    {
        return $this->tarif;
    }

    public function setTarif(?Tarif $tarif): self
    {
        $this->tarif = $tarif;

        // set (or unset) the owning side of the relation if necessary
        $newPromo = $tarif === null ? null : $this;
        if ($newPromo !== $tarif->getPromo()) {
            $tarif->setPromo($newPromo);
        }

        return $this;
    }
}
