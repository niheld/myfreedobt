<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeClientRepository")
 * @ApiResource(attributes={"pagination_enabled"=false},
 *              normalizationContext={"groups"={"type_client"}})
 */
class TypeClient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("type_client")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("type_client")
     * @Groups("per_morale")
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Client", mappedBy="type")
     */
    private $clients;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PerMorale", mappedBy="type")
     * @Groups("type_client")
     */
    private $perMorales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacts", mappedBy="type")
     */
    private $contacts;

    public function __construct()
    {
        $this->clients = new ArrayCollection();
        $this->perMorales = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->setType($this);
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->clients->contains($client)) {
            $this->clients->removeElement($client);
            // set the owning side to null (unless already changed)
            if ($client->getType() === $this) {
                $client->setType(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'entité'
        return $this->getLibelle();
        // Pour afficher l'id de la ville : return $this->$nom;
        
    }

    /**
     * @return Collection|PerMorale[]
     */
    public function getPerMorales(): Collection
    {
        return $this->perMorales;
    }

    public function addPerMorale(PerMorale $perMorale): self
    {
        if (!$this->perMorales->contains($perMorale)) {
            $this->perMorales[] = $perMorale;
            $perMorale->setType($this);
        }

        return $this;
    }

    public function removePerMorale(PerMorale $perMorale): self
    {
        if ($this->perMorales->contains($perMorale)) {
            $this->perMorales->removeElement($perMorale);
            // set the owning side to null (unless already changed)
            if ($perMorale->getType() === $this) {
                $perMorale->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setType($this);
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getType() === $this) {
                $contact->setType(null);
            }
        }

        return $this;
    }

}
