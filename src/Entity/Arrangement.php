<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArrangementRepository")
 * @ApiResource
 */
class Arrangement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chambrehotel", mappedBy="arrangement")
     */
    private $chambrehotels;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Promo", mappedBy="arrangement")
     */
    private $promos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Saison", mappedBy="arrangement")
     */
    private $saisons;

    public function __construct()
    {
        $this->chambrehotels = new ArrayCollection();
        $this->promos = new ArrayCollection();
        $this->saisons = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Chambrehotel[]
     */
    public function getChambrehotels(): Collection
    {
        return $this->chambrehotels;
    }

    public function addChambrehotel(Chambrehotel $chambrehotel): self
    {
        if (!$this->chambrehotels->contains($chambrehotel)) {
            $this->chambrehotels[] = $chambrehotel;
            $chambrehotel->setArrangement($this);
        }

        return $this;
    }

    public function removeChambrehotel(Chambrehotel $chambrehotel): self
    {
        if ($this->chambrehotels->contains($chambrehotel)) {
            $this->chambrehotels->removeElement($chambrehotel);
            // set the owning side to null (unless already changed)
            if ($chambrehotel->getArrangement() === $this) {
                $chambrehotel->setArrangement(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return $this->getNom();
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }

    /**
     * @return Collection|Promo[]
     */
    public function getPromos(): Collection
    {
        return $this->promos;
    }

    public function addPromo(Promo $promo): self
    {
        if (!$this->promos->contains($promo)) {
            $this->promos[] = $promo;
            $promo->addArrangement($this);
        }

        return $this;
    }

    public function removePromo(Promo $promo): self
    {
        if ($this->promos->contains($promo)) {
            $this->promos->removeElement($promo);
            $promo->removeArrangement($this);
        }

        return $this;
    }

    /**
     * @return Collection|Saison[]
     */
    public function getSaisons(): Collection
    {
        return $this->saisons;
    }

    public function addSaison(Saison $saison): self
    {
        if (!$this->saisons->contains($saison)) {
            $this->saisons[] = $saison;
            $saison->addArrangement($this);
        }

        return $this;
    }

    public function removeSaison(Saison $saison): self
    {
        if ($this->saisons->contains($saison)) {
            $this->saisons->removeElement($saison);
            $saison->removeArrangement($this);
        }

        return $this;
    }
}
