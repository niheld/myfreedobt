<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StopsaleRepository")
 * @ApiResource
 * @ApiFilter(DateFilter::class, properties={"reprise"})
 */
class Stopsale
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $debut;

    /**
     * @ORM\Column(type="date")
     */
    private $reprise;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="stopsales")
     * @ORM\JoinColumn(nullable=false)
     */
    private $hotel;

    public function __construct(){
        $this->debut = new \DateTime();
        $this->reprise = new \DateTime();

    }

    public function getId()
    {
        return $this->id;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getReprise(): ?\DateTimeInterface
    {
        return $this->reprise;
    }

    public function setReprise(\DateTimeInterface $reprise): self
    {
        $this->reprise = $reprise;

        return $this;
    }

    public function getHotel(): ?Hotel
    {
        return $this->hotel;
    }

    public function setHotel(?Hotel $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }
    public function getDate(){
        return $this->date = new \DateTime();

    }
}
