<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoyagesRepository")
 * @ApiResource
 */
class Voyages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $img;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrejour;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrenuitee;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $inclu1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $inclu2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $inclu3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $inclu4;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getNbrejour(): ?int
    {
        return $this->nbrejour;
    }

    public function setNbrejour(int $nbrejour): self
    {
        $this->nbrejour = $nbrejour;

        return $this;
    }

    public function getNbrenuitee(): ?int
    {
        return $this->nbrenuitee;
    }

    public function setNbrenuitee(int $nbrenuitee): self
    {
        $this->nbrenuitee = $nbrenuitee;

        return $this;
    }

    public function getInclu1(): ?string
    {
        return $this->inclu1;
    }

    public function setInclu1(?string $inclu1): self
    {
        $this->inclu1 = $inclu1;

        return $this;
    }

    public function getInclu2(): ?string
    {
        return $this->inclu2;
    }

    public function setInclu2(?string $inclu2): self
    {
        $this->inclu2 = $inclu2;

        return $this;
    }

    public function getInclu3(): ?string
    {
        return $this->inclu3;
    }

    public function setInclu3(?string $inclu3): self
    {
        $this->inclu3 = $inclu3;

        return $this;
    }

    public function getInclu4(): ?string
    {
        return $this->inclu4;
    }

    public function setInclu4(?string $inclu4): self
    {
        $this->inclu4 = $inclu4;

        return $this;
    }
}
