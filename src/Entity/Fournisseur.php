<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FournisseurRepository")
 * @ApiResource
 */
class Fournisseur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mf;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rib;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeFournisseur", inversedBy="fournisseurs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeFournisseur;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PayementFournisseur", mappedBy="fournisseur")
     */
    private $payementFournisseurs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ventebillet", mappedBy="fournisseur")
     */
    private $ventebillets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cheque", mappedBy="fournisseur")
     */
    private $cheques;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promo", inversedBy="fournisseur")
     */
    private $promo;

    public function __construct()
    {
        $this->payementFournisseurs = new ArrayCollection();
        $this->ventebillets = new ArrayCollection();
        $this->cheques = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getMf(): ?string
    {
        return $this->mf;
    }

    public function setMf(?string $mf): self
    {
        $this->mf = $mf;

        return $this;
    }

    public function getRib(): ?string
    {
        return $this->rib;
    }

    public function setRib(?string $rib): self
    {
        $this->rib = $rib;

        return $this;
    }

    public function getTypeFournisseur(): ?TypeFournisseur
    {
        return $this->typeFournisseur;
    }

    public function setTypeFournisseur(?TypeFournisseur $typeFournisseur): self
    {
        $this->typeFournisseur = $typeFournisseur;

        return $this;
    }

    /**
     * @return Collection|PayementFournisseur[]
     */
    public function getPayementFournisseurs(): Collection
    {
        return $this->payementFournisseurs;
    }

    public function addPayementFournisseur(PayementFournisseur $payementFournisseur): self
    {
        if (!$this->payementFournisseurs->contains($payementFournisseur)) {
            $this->payementFournisseurs[] = $payementFournisseur;
            $payementFournisseur->setFournisseur($this);
        }

        return $this;
    }

    public function removePayementFournisseur(PayementFournisseur $payementFournisseur): self
    {
        if ($this->payementFournisseurs->contains($payementFournisseur)) {
            $this->payementFournisseurs->removeElement($payementFournisseur);
            // set the owning side to null (unless already changed)
            if ($payementFournisseur->getFournisseur() === $this) {
                $payementFournisseur->setFournisseur(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return $this->getNom();
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }

    /**
     * @return Collection|Ventebillet[]
     */
    public function getVentebillets(): Collection
    {
        return $this->ventebillets;
    }

    public function addVentebillet(Ventebillet $ventebillet): self
    {
        if (!$this->ventebillets->contains($ventebillet)) {
            $this->ventebillets[] = $ventebillet;
            $ventebillet->setFournisseur($this);
        }

        return $this;
    }

    public function removeVentebillet(Ventebillet $ventebillet): self
    {
        if ($this->ventebillets->contains($ventebillet)) {
            $this->ventebillets->removeElement($ventebillet);
            // set the owning side to null (unless already changed)
            if ($ventebillet->getFournisseur() === $this) {
                $ventebillet->setFournisseur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cheque[]
     */
    public function getCheques(): Collection
    {
        return $this->cheques;
    }

    public function addCheque(Cheque $cheque): self
    {
        if (!$this->cheques->contains($cheque)) {
            $this->cheques[] = $cheque;
            $cheque->setAuprofit($this);
        }

        return $this;
    }

    public function removeCheque(Cheque $cheque): self
    {
        if ($this->cheques->contains($cheque)) {
            $this->cheques->removeElement($cheque);
            // set the owning side to null (unless already changed)
            if ($cheque->getAuprofit() === $this) {
                $cheque->setAuprofit(null);
            }
        }

        return $this;
    }

    public function getPromo(): ?Promo
    {
        return $this->promo;
    }

    public function setPromo(?Promo $promo): self
    {
        $this->promo = $promo;

        return $this;
    }
}
