<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactsRepository")
 * @ApiResource(attributes={"pagination_enabled"=false},
 *              normalizationContext={"groups"={"contacts"}})
 */
class Contacts
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("contacts")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeClient", inversedBy="contacts")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PerMorale", inversedBy="contacts")
     * @Groups("contacts")
     */
    private $perMorale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("contacts")
     * @Groups("per_morale")
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("contacts")
     * @Groups("per_morale")
     */
    private $tel1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("contacts")
     * @Groups("per_morale")
     */
    private $tel2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("contacts")
     * @Groups("per_morale")
     */
    private $mail;

    public function getId()
    {
        return $this->id;
    }

    public function getType(): ?TypeClient
    {
        return $this->type;
    }

    public function setType(?TypeClient $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPerMorale(): ?PerMorale
    {
        return $this->perMorale;
    }

    public function setPerMorale(?PerMorale $perMorale): self
    {
        $this->perMorale = $perMorale;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(?string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(?string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return $this->getLibelle();
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }
}
