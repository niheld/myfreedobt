<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @ApiResource
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VenteHotel", mappedBy="client")
     */
    private $venteHotels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ventebillet", mappedBy="client")
     */
    private $ventebillets;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeClient", inversedBy="clients")
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Demande", mappedBy="client")
     */
    private $demandes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ClientIndiv", mappedBy="b2b")
     */
    private $clientIndivs;

    public function __construct()
    {
        $this->venteHotels = new ArrayCollection();
        $this->ventebillets = new ArrayCollection();
        $this->demandes = new ArrayCollection();
        $this->clientIndivs = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTel(): ?int
    {
        return $this->tel;
    }

    public function setTel(int $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * @return Collection|VenteHotel[]
     */
    public function getVenteHotels(): Collection
    {
        return $this->venteHotels;
    }

    public function addVenteHotel(VenteHotel $venteHotel): self
    {
        if (!$this->venteHotels->contains($venteHotel)) {
            $this->venteHotels[] = $venteHotel;
            $venteHotel->setClient($this);
        }

        return $this;
    }

    public function removeVenteHotel(VenteHotel $venteHotel): self
    {
        if ($this->venteHotels->contains($venteHotel)) {
            $this->venteHotels->removeElement($venteHotel);
            // set the owning side to null (unless already changed)
            if ($venteHotel->getClient() === $this) {
                $venteHotel->setClient(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'entité'
        return $this->getNom();
        // Pour afficher l'id de la ville : return $this->$nom;
        
    }

    /**
     * @return Collection|Ventebillet[]
     */
    public function getVentebillets(): Collection
    {
        return $this->ventebillets;
    }

    public function addVentebillet(Ventebillet $ventebillet): self
    {
        if (!$this->ventebillets->contains($ventebillet)) {
            $this->ventebillets[] = $ventebillet;
            $ventebillet->setClient($this);
        }

        return $this;
    }

    public function removeVentebillet(Ventebillet $ventebillet): self
    {
        if ($this->ventebillets->contains($ventebillet)) {
            $this->ventebillets->removeElement($ventebillet);
            // set the owning side to null (unless already changed)
            if ($ventebillet->getClient() === $this) {
                $ventebillet->setClient(null);
            }
        }

        return $this;
    }

    public function getType(): ?TypeClient
    {
        return $this->type;
    }

    public function setType(?TypeClient $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Demande[]
     */
    public function getDemandes(): Collection
    {
        return $this->demandes;
    }

    public function addDemande(Demande $demande): self
    {
        if (!$this->demandes->contains($demande)) {
            $this->demandes[] = $demande;
            $demande->addClient($this);
        }

        return $this;
    }

    public function removeDemande(Demande $demande): self
    {
        if ($this->demandes->contains($demande)) {
            $this->demandes->removeElement($demande);
            $demande->removeClient($this);
        }

        return $this;
    }

    /**
     * @return Collection|ClientIndiv[]
     */
    public function getClientIndivs(): Collection
    {
        return $this->clientIndivs;
    }

    public function addClientIndiv(ClientIndiv $clientIndiv): self
    {
        if (!$this->clientIndivs->contains($clientIndiv)) {
            $this->clientIndivs[] = $clientIndiv;
            $clientIndiv->addB2b($this);
        }

        return $this;
    }

    public function removeClientIndiv(ClientIndiv $clientIndiv): self
    {
        if ($this->clientIndivs->contains($clientIndiv)) {
            $this->clientIndivs->removeElement($clientIndiv);
            $clientIndiv->removeB2b($this);
        }

        return $this;
    }
}
