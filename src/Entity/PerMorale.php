<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PerMoraleRepository")
 * @ApiResource(attributes={"pagination_enabled"=false},
 *              normalizationContext={"groups"={"per_morale"}})
 */
class PerMorale
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("per_morale")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeClient", inversedBy="perMorales")
     * @Groups("per_morale")
     * @Groups("type_client")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("per_morale")
     * @Groups("contacts")
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("per_morale")
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $respensable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mat_fiscale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rib;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("per_morale")
     */
    private $website;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pays", inversedBy="perMorales")
     * @Groups("per_morale")
     */
    private $pays;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacts", mappedBy="perMorale")
     * @Groups("per_morale")
     */
    private $contacts;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType(): ?TypeClient
    {
        return $this->type;
    }

    public function setType(?TypeClient $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getRespensable(): ?string
    {
        return $this->respensable;
    }

    public function setRespensable(?string $respensable): self
    {
        $this->respensable = $respensable;

        return $this;
    }

    public function getMatFiscale(): ?string
    {
        return $this->mat_fiscale;
    }

    public function setMatFiscale(?string $mat_fiscale): self
    {
        $this->mat_fiscale = $mat_fiscale;

        return $this;
    }

    public function getRib(): ?string
    {
        return $this->rib;
    }

    public function setRib(?string $rib): self
    {
        $this->rib = $rib;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    public function setPays(?Pays $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setPerMorale($this);
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getPerMorale() === $this) {
                $contact->setPerMorale(null);
            }
        }

        return $this;
    }
    public function __toString(){
        // sert a afficher le nom de l'hotel
        return $this->getLibelle();
        // Pour afficher l'id de l'hotel : return $this->$nom;
        
    }
}
