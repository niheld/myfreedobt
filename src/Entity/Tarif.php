<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
//use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TarifRepository")
 * @ApiResource
 */
class Tarif
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="tarifs")
     */
    private $user;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Promo", inversedBy="tarif", cascade={"persist", "remove"})
     */
    private $promo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $lpd_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ls_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $dp_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $dpplus_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pc_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pcplus_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $allinsoft_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $allin_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $ultraallin_achat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $supp_sing_achat;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $gratuite_1enf;

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->user = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
        }

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPromo(): ?Promo
    {
        return $this->promo;
    }

    public function setPromo(?Promo $promo): self
    {
        $this->promo = $promo;

        return $this;
    }

    public function getLpdAchat(): ?float
    {
        return $this->lpd_achat;
    }

    public function setLpdAchat(?float $lpd_achat): self
    {
        $this->lpd_achat = $lpd_achat;

        return $this;
    }

    public function getLsAchat(): ?float
    {
        return $this->ls_achat;
    }

    public function setLsAchat(float $ls_achat): self
    {
        $this->ls_achat = $ls_achat;

        return $this;
    }

    public function getDpAchat(): ?float
    {
        return $this->dp_achat;
    }

    public function setDpAchat(?float $dp_achat): self
    {
        $this->dp_achat = $dp_achat;

        return $this;
    }

    public function getDpplusAchat(): ?float
    {
        return $this->dpplus_achat;
    }

    public function setDpplusAchat(?float $dpplus_achat): self
    {
        $this->dpplus_achat = $dpplus_achat;

        return $this;
    }

    public function getPcAchat(): ?float
    {
        return $this->pc_achat;
    }

    public function setPcAchat(?float $pc_achat): self
    {
        $this->pc_achat = $pc_achat;

        return $this;
    }

    public function getPcplusAchat(): ?float
    {
        return $this->pcplus_achat;
    }

    public function setPcplusAchat(?float $pcplus_achat): self
    {
        $this->pcplus_achat = $pcplus_achat;

        return $this;
    }

    public function getAllinsoftAchat(): ?float
    {
        return $this->allinsoft_achat;
    }

    public function setAllinsoftAchat(?float $allinsoft_achat): self
    {
        $this->allinsoft_achat = $allinsoft_achat;

        return $this;
    }

    public function getAllinAchat(): ?float
    {
        return $this->allin_achat;
    }

    public function setAllinAchat(?float $allin_achat): self
    {
        $this->allin_achat = $allin_achat;

        return $this;
    }

    public function getUltraallinAchat(): ?float
    {
        return $this->ultraallin_achat;
    }

    public function setUltraallinAchat(?float $ultraallin_achat): self
    {
        $this->ultraallin_achat = $ultraallin_achat;

        return $this;
    }

    public function getSuppSingAchat(): ?float
    {
        return $this->supp_sing_achat;
    }

    public function setSuppSingAchat(?float $supp_sing_achat): self
    {
        $this->supp_sing_achat = $supp_sing_achat;

        return $this;
    }

    public function getGratuite1enf(): ?int
    {
        return $this->gratuite_1enf;
    }

    public function setGratuite1enf(?int $gratuite_1enf): self
    {
        $this->gratuite_1enf = $gratuite_1enf;

        return $this;
    }
}
